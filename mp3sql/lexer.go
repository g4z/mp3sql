package mp3sql

import (
	"errors"
	"unicode"
)

const (
	TokenTypeKeyword = iota
	TokenTypeField
	TokenTypeString
	TokenTypeNumber
	TokenTypeSymbol
	TokenTypeSource
)

type Lexer struct {
	reader *Reader
}

type Token struct {
	Type   int
	Value  string
	Length int
}

func NewLexer(sql string) (*Lexer, error) {

	reader, err := NewReader(sql)
	if err != nil {
		return nil, err
	}

	return &Lexer{
		reader: reader,
	}, nil
}

func (l *Lexer) UngetToken(token *Token) {
	for i := 0; i < token.Length; i++ {
		l.reader.UngetChar()
	}
}

func (l *Lexer) GetToken() (*Token, error) {

	var token *Token

	// get the first character
	char, err := l.reader.GetChar()
	if err != nil {
		return nil, err
	}

	// remove any whitespace
	for unicode.IsSpace(*char) {
		char, err = l.reader.GetChar()
		if err != nil {
			return nil, err
		}
	}

	if unicode.IsNumber(*char) {
		l.reader.UngetChar()
		token, err = l.parseNumericToken()
	} else if unicode.IsSymbol(*char) || unicode.IsPunct(*char) {
		if *char == '"' || *char == '\'' {
			l.reader.UngetChar()
			token, err = l.parseStringToken()
		} else if *char == '/' {
			l.reader.UngetChar()
			token, err = l.parseSourceToken()
		} else if *char == '~' {
			char, err = l.reader.GetChar()
			if err != nil {
				return nil, err
			}
			l.reader.UngetChar()
			l.reader.UngetChar()
			if *char == '/' {
				token, err = l.parseSourceToken()
			}
		} else {
			l.reader.UngetChar()
			token, err = l.parseSymbolToken()
		}
	} else {
		l.reader.UngetChar()
		token, err = l.parseIdentifierToken()
	}

	return token, err
}

func (l *Lexer) parseNumericToken() (*Token, error) {

	var char *rune
	var err error
	var buffer = ""

ReadTokenLoop:
	for !l.reader.Eof() {

		char, err = l.reader.GetChar()
		if err != nil {
			return nil, err
		}

		if !unicode.IsNumber(*char) {
			l.reader.UngetChar()
			break ReadTokenLoop
		}

		buffer += string(*char)
	}

	token := &Token{
		Type:   TokenTypeNumber,
		Value:  buffer,
		Length: len(buffer),
	}

	return token, nil
}

func (l *Lexer) parseSymbolToken() (*Token, error) {

	var char *rune
	var err error
	var buffer = ""

ReadTokenLoop:
	for !l.reader.Eof() {

		char, err = l.reader.GetChar()
		if err != nil {
			return nil, err
		}

		switch *char {
		case '*':
			buffer += string(*char)
			break ReadTokenLoop
		case ',':
			buffer += string(*char)
			break ReadTokenLoop
		case '=', '~':
			buffer += string(*char)
			break ReadTokenLoop
		case '!':
			buffer += string(*char)
			char, err = l.reader.GetChar()
			if err != nil {
				return nil, err
			}
			switch *char {
			case '=', '~':
				buffer += string(*char)
			default:
				l.reader.UngetChar()
			}
			break ReadTokenLoop
		case '>', '<':
			buffer += string(*char)
			char, err = l.reader.GetChar()
			if err != nil {
				return nil, err
			}
			switch *char {
			case '=':
				buffer += string(*char)
			default:
				l.reader.UngetChar()
			}
			break ReadTokenLoop
		default:
			return nil, errors.New("Unsupported symbol: " + string(*char))
		}
	}

	token := &Token{
		Type:   TokenTypeSymbol,
		Value:  buffer,
		Length: len(buffer),
	}

	return token, nil
}

func (l *Lexer) parseSourceToken() (*Token, error) {

	var char *rune
	var err error
	var buffer = ""

ReadTokenLoop:
	for !l.reader.Eof() {

		char, err = l.reader.GetChar()
		if err != nil {
			return nil, err
		}

		if unicode.IsSpace(*char) {
			break ReadTokenLoop
		}

		buffer += string(*char)
	}
	token := &Token{
		Type:   TokenTypeSource,
		Value:  buffer,
		Length: len(buffer),
	}

	return token, nil

}

func (l *Lexer) parseStringToken() (*Token, error) {

	var char, match *rune
	var err error
	var buffer = ""

	// get the string delimiter char
	char, err = l.reader.GetChar()
	if err != nil {
		return nil, err
	}

	match = char

ReadTokenLoop:
	for !l.reader.Eof() {
		char, err = l.reader.GetChar()
		if err != nil {
			return nil, err
		}

		if *char == *match {
			break ReadTokenLoop
		}

		buffer += string(*char)
	}

	token := &Token{
		Type:   TokenTypeString,
		Value:  buffer,
		Length: len(buffer),
	}

	return token, nil
}

func (l *Lexer) parseIdentifierToken() (*Token, error) {

	var char *rune
	var err error
	var buffer = ""
	var tokenType int

ReadTokenLoop:
	for !l.reader.Eof() {

		char, err = l.reader.GetChar()
		if err != nil {
			return nil, err
		}

		if unicode.IsSpace(*char) {
			l.reader.UngetChar()
			break ReadTokenLoop
		}

		if unicode.IsSymbol(*char) || unicode.IsPunct(*char) {
			l.reader.UngetChar()
			break ReadTokenLoop
		}

		buffer += string(*char)
	}

	tokenType = TokenTypeField
	switch buffer {
	case "SELECT",
		"UPDATE",
		"SET",
		"FROM",
		"WHERE",
		"OR",
		"AND",
		"ORDER",
		"BY",
		"ASC",
		"DESC",
		"LIMIT":
		tokenType = TokenTypeKeyword
	}

	token := &Token{
		Type:   tokenType,
		Value:  buffer,
		Length: len(buffer),
	}

	return token, nil
}
