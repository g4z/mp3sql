package mp3sql

import (
	"testing"
)

func TestGetToken(t *testing.T) {

	var sql = `
SELECT
	artist,title
FROM ~/Music
WHERE
	genre='House'
	AND year>=2000
ORDER BY artist ASC
LIMIT 10
`

	var lexer *Lexer
	var token *Token
	var err error

	lexer, err = NewLexer(sql)
	if err != nil {
		t.Error(err)
	}

	// Test for SELECT

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Length != 6 {
		t.Error("Expected length 6 for SELECT but found length", token.Length)
	}

	if token.Value != "SELECT" {
		t.Error("Expected SELECT but found", token.Value)
	}

	if token.Type != TokenTypeKeyword {
		t.Error("Expected type TokenTypeKeyword for SELECT")
	}

	// Test for artist field

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "artist" {
		t.Error("Expected 'artist' but found", token.Value)
	}

	if token.Type != TokenTypeField {
		t.Error("Expected type TokenTypeField for artist")
	}
	// Test for comma symbol

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "," {
		t.Error("Expected , but found", token.Value)
	}

	if token.Type != TokenTypeSymbol {
		t.Error("Expected type TokenTypeSymbol for ,")
	}

	// Test for title field

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "title" {
		t.Error("Expected 'title' but found", token.Value)
	}

	if token.Type != TokenTypeField {
		t.Error("Expected type TokenTypeField for title")
	}

	// Test for FROM keyword

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "FROM" {
		t.Error("Expected FROM but found", token.Value)
	}

	if token.Type != TokenTypeKeyword {
		t.Error("Expected type TokenTypeKeyword for FROM")
	}

	// Test for SOURCE

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "~/Music" {
		t.Error("Expected ~/Music but found", token.Value)
	}

	if token.Type != TokenTypeSource {
		t.Error("Expected type TokenTypeSource for ~/Music, not", TokenTypeSource)
	}

	// Test for WHERE

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "WHERE" {
		t.Error("Expected WHERE but found", token.Value)
	}

	if token.Type != TokenTypeKeyword {
		t.Error("Expected type TokenTypeKeyword for WHERE")
	}

	// Test for 'genre'

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "genre" {
		t.Error("Expected 'genre' but found", token.Value)
	}

	if token.Type != TokenTypeField {
		t.Error("Expected type TokenTypeField for 'genre'")
	}

	// Test for 'genre' operator

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "=" {
		t.Error("Expected '=' but found", token.Value)
	}

	if token.Type != TokenTypeSymbol {
		t.Error("Expected type TokenTypeSymbol for =")
	}

	// Test for 'genre' value

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "House" {
		t.Error("Expected 'House' but found", token.Value)
	}

	if token.Type != TokenTypeString {
		t.Error("Expected type TokenTypeString for 'House'")
	}

	// Test for AND

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "AND" {
		t.Error("Expected AND but found", token.Value)
	}

	if token.Type != TokenTypeKeyword {
		t.Error("Expected type TokenTypeKeyword for AND")
	}

	// Test for 'year'

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "year" {
		t.Error("Expected 'year' but found", token.Value)
	}

	if token.Type != TokenTypeField {
		t.Error("Expected type TokenTypeField for 'genre'")
	}

	// Test for 'year' operator

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != ">=" {
		t.Error("Expected >= but found", token.Value)
	}

	if token.Type != TokenTypeSymbol {
		t.Error("Expected type TokenTypeSymbol for >=")
	}

	// Test for 'year' value

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "2000" {
		t.Error("Expected 2000 but found", token.Value)
	}

	if token.Type != TokenTypeNumber {
		t.Error("Expected type TokenTypeNumber for 2000")
	}

	// Test for ORDER BY

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "ORDER" {
		t.Error("Expected ORDER but found", token.Value)
	}

	if token.Type != TokenTypeKeyword {
		t.Error("Expected type TokenTypeKeyword for ORDER")
	}

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "BY" {
		t.Error("Expected BY but found", token.Value)
	}

	if token.Type != TokenTypeKeyword {
		t.Error("Expected type TokenTypeKeyword for BY")
	}

	// Test for 'artist'

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "artist" {
		t.Error("Expected artist but found", token.Value)
	}

	if token.Type != TokenTypeField {
		t.Error("Expected type TokenTypeField for 'artist'")
	}

	// Test for 'ASC'

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "ASC" {
		t.Error("Expected ASC but found", token.Value)
	}

	if token.Type != TokenTypeKeyword {
		t.Error("Expected type TokenTypeKeyword for ASC")
	}

	// Test for 'LIMIT'

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "LIMIT" {
		t.Error("Expected LIMIT but found", token.Value)
	}

	if token.Type != TokenTypeKeyword {
		t.Error("Expected type TokenTypeKeyword for LIMIT")
	}

	// Test for LIMIT values

	token, err = lexer.GetToken()
	if err != nil {
		t.Error(err)
	}

	if token.Value != "10" {
		t.Error("Expected 10 but found", token.Value)
	}

	if token.Type != TokenTypeNumber {
		t.Error("Expected type TokenTypeNumber for 10")
	}

}
