package mp3sql

import (
	"testing"
)

func TestParse1(t *testing.T) {

	var sql = `SELECT artist,title FROM ~/Music WHERE genre='House' AND year>=2000 ORDER BY artist ASC LIMIT 10`

	statement := doTestParse(sql, t)

	doCheckSelectComponent(
		[]string{"artist", "title"},
		&statement.SelectComponent,
		t,
	)

	doCheckFromComponent(
		[]string{"~/Music"},
		&statement.FromComponent,
		t,
	)

	doCheckWhereComponent(
		[][]string{{"genre", "=", "House"}, {"year", ">=", "2000"}},
		&statement.WhereComponent,
		t,
	)

	doCheckOrderComponent(
		[][]string{{"artist", "ASC"}},
		&statement.OrderComponent,
		t,
	)

	doCheckLimitComponent(
		[]int{0, 10},
		&statement.LimitComponent,
		t,
	)
}

func TestParse2(t *testing.T) {

	var sql = `SELECT * FROM ~/Music,/music ORDER BY title DESC LIMIT 50,10`

	statement := doTestParse(sql, t)

	doCheckSelectComponent(
		[]string{"*"},
		&statement.SelectComponent,
		t,
	)

	doCheckFromComponent(
		// TODO convert this in an array
		[]string{"~/Music,/music"},
		&statement.FromComponent,
		t,
	)

	doCheckWhereComponent(
		[][]string{},
		&statement.WhereComponent,
		t,
	)

	doCheckOrderComponent(
		[][]string{{"title", "DESC"}},
		&statement.OrderComponent,
		t,
	)

	doCheckLimitComponent(
		[]int{50, 10},
		&statement.LimitComponent,
		t,
	)
}

func doTestParse(sql string, t *testing.T) *SelectStatement {

	var parser *Parser
	var statement *SelectStatement
	var err error

	parser, err = NewParser(sql)
	if err != nil {
		t.Error(err)
	}

	statement, err = parser.Parse()
	if err != nil {
		t.Error(err)
	}

	return statement
}

func doCheckSelectComponent(expected []string, component *SelectComponent, t *testing.T) {
	for i := 0; i < len(expected); i++ {
		if i >= len(component.Fields) {
			t.Error("Expected field " + expected[i])
			return
		}
		field := component.Fields[i]
		if expected[i] != field.Name {
			t.Error("Expected " + expected[i] + " but found " + field.Name)
		}
	}
}

func doCheckFromComponent(expected []string, component *FromComponent, t *testing.T) {
	for i := 0; i < len(expected); i++ {
		if i >= len(component.Sources) {
			t.Error("Expected source " + expected[i])
			return
		}
		source := component.Sources[i]
		if expected[i] != source.Path {
			t.Error("Expected " + expected[i] + " but found " + source.Path)
		}
	}
}

func doCheckWhereComponent(expected [][]string, component *WhereComponent, t *testing.T) {
	for i := 0; i < len(expected); i++ {
		if i >= len(component.Filters) {
			t.Error("Expected filter " + expected[i][0])
			return
		}
		filter := component.Filters[i]
		if filter.Field != expected[i][0] {
			t.Error("Expected " + expected[i][0] + " but found " + filter.Field)
		}
		if filter.Operator != expected[i][1] {
			t.Error("Expected " + expected[i][1] + " but found " + filter.Operator)
		}
		if filter.Value != expected[i][2] {
			t.Error("Expected " + expected[i][2] + " but found " + filter.Value)
		}
	}
}

func doCheckOrderComponent(expected [][]string, component *OrderComponent, t *testing.T) {
	for i := 0; i < len(expected); i++ {
		if i >= len(component.Orders) {
			t.Error("Expected order by " + expected[i][0])
			return
		}
		order := component.Orders[i]
		if order.Field != expected[i][0] {
			t.Error("Expected " + expected[i][0] + " but found " + order.Field)
		}
		if order.Direction != expected[i][1] {
			t.Error("Expected " + expected[i][1] + " but found " + order.Direction)
		}
	}
}

func doCheckLimitComponent(expected []int, component *LimitComponent, t *testing.T) {
	limit := component.Limits[0]
	if expected[0] != limit.From {
		t.Error("Expected " + string(expected[0]) + " but found " + string(limit.From))
	}
	if expected[1] != limit.Max {
		t.Error("Expected " + string(expected[1]) + " but found " + string(limit.Max))
	}
}
