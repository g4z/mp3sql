package mp3sql

import (
	"fmt"
	"os"
)

// Dump dumps objects
// for debugging. like Laravels `dump`
func Dump(args ...interface{}) {
	for _, arg := range args {
		fmt.Printf("%+v\n", arg)
	}
}

// Dumpd dumps objects and exits
// for debugging. like Laravels `dd`
func Dumpd(args ...interface{}) {
	for _, arg := range args {
		fmt.Printf("%+v\n", arg)
	}
	os.Exit(1)
}
