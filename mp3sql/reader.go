package mp3sql

import (
	"io"
)

type Reader struct {
	buffer string
	length int
	offset int
}

func NewReader(sql string) (*Reader, error) {
	return &Reader{
		buffer: sql,
		length: len(sql),
		offset: 0,
	}, nil
}

func (r *Reader) Eof() bool {
	return r.offset >= r.length
}

func (r *Reader) GetChar() (*rune, error) {
	if r.Eof() {
		// println("GetChar -> Found EOF")
		return nil, io.EOF
	}
	char := rune(r.buffer[r.offset])

	// println("GetChar -> Read char: '" + string(char) + "' at offset " + strconv.Itoa(r.offset))
	r.offset++
	// println("GetChar -> Offset incrmented to " + strconv.Itoa(r.offset))
	return &char, nil
}

func (r *Reader) UngetChar() {
	// println("UngetChar -> Unget char: '" + string(r.buffer[r.offset]) + "' at offset " + strconv.Itoa(r.offset))
	r.offset--
	// println("GetChar -> Offset decremented to " + strconv.Itoa(r.offset))
}

func (r *Reader) Length() int {
	return len(r.buffer)
}

func (r *Reader) Offset() int {
	return r.offset
}
