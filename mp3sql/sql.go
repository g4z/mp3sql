package mp3sql

//========= SELECT ==========

type Field struct {
	Name string
}

type SelectComponent struct {
	Fields []Field
}

//========= FROM ==========

type Source struct {
	Path string
}

type FromComponent struct {
	Sources []Source
}

//========= WHERE ==========

type Filter struct {
	Field    string
	Operator string
	Value    string
}

type WhereComponent struct {
	Filters []Filter
}

//========= ORDER ==========

const OrderASC = "ASC"
const OrderDESC = "DESC"

type Order struct {
	Field     string
	Direction string
}

type OrderComponent struct {
	Orders []Order
}

//========= LIMIT ==========

type Limit struct {
	From int
	Max  int
}

type LimitComponent struct {
	Limits []Limit
}

//======================================

// type UpdateComponent struct{}
// type SetComponent struct{}

type SelectStatement struct {
	SelectComponent
	FromComponent
	WhereComponent
	OrderComponent
	LimitComponent
}
