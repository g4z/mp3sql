package mp3sql

import (
	"errors"
	"io"
	"strconv"
)

type Parser struct {
	lexer *Lexer
}

func NewParser(sql string) (*Parser, error) {

	lexer, err := NewLexer(sql)
	if err != nil {
		return nil, err
	}

	return &Parser{
		lexer: lexer,
	}, nil
}

func (p *Parser) Parse() (*SelectStatement, error) {

	var statement *SelectStatement

	token, err := p.lexer.GetToken()
	if err != nil {
		return nil, errors.New("Syntax Error: " + err.Error())
	}

	switch token.Value {
	case "SELECT":
		statement, err = p.parseSelectStatement(token)
	case "UPDATE":
		statement, err = p.parseUpdateStatement(token)
	default:
		statement, err = nil, errors.New("Syntax Error: Only SELECT and UPDATE are supported")
	}

	return statement, err
}

func (p *Parser) parseSelectStatement(token *Token) (*SelectStatement, error) {

	var err error

	statement := &SelectStatement{
		// Action: token.Value,
	}

ReadFieldListLoop:
	for { // read in select fields

		token, err = p.lexer.GetToken()
		if err != nil {
			if err == io.EOF {
				err = errors.New("EOF in SELECT statement")
			}
			return statement, err
		}

		switch token.Type {
		case TokenTypeField:
			statement.SelectComponent.Fields = append(
				statement.SelectComponent.Fields,
				Field{token.Value},
			)
		case TokenTypeSymbol:
			switch token.Value {
			case "*":
				statement.SelectComponent.Fields = append(
					statement.SelectComponent.Fields,
					Field{token.Value},
				)
			case ",":
				break
			default:
				return statement, errors.New("Unexpected " + string(token.Value))
			}
		default:
			break ReadFieldListLoop
		}
	}

	if token.Value != "FROM" {
		return statement, errors.New("Expected FROM keyword")
	}

	token, err = p.lexer.GetToken()
	if err != nil {
		if err == io.EOF {
			err = errors.New("EOF in FROM statement")
		}
		return statement, err
	}

	statement.FromComponent.Sources = append(
		statement.FromComponent.Sources,
		Source{token.Value},
	)

	token, err = p.lexer.GetToken()
	if err != nil {
		if err == io.EOF {
			err = nil
		}
		return statement, err
	}

	if token.Value == "WHERE" {
		whereClause, err := p.parseWhereClause()
		if err != nil {
			if err == io.EOF {
				err = errors.New("EOF in WHERE statement")
			}
			return statement, err
		}
		statement.WhereComponent = *whereClause
		token, err = p.lexer.GetToken()
		if err != nil {
			if err == io.EOF {
				err = nil
			}
			return statement, err
		}
	}

	if token.Value == "ORDER" {
		orderClause, err := p.parseOrderClause()
		if err != nil {
			return statement, err
		}
		statement.OrderComponent = *orderClause
		token, err = p.lexer.GetToken()
		if err != nil {
			if err == io.EOF {
				err = nil
			}
			return statement, err
		}
	}

	if token.Value == "LIMIT" {
		limitClause, err := p.parseLimitClause()
		if err != nil {
			return statement, err
		}
		statement.LimitComponent = *limitClause
	}

	return statement, nil
}

func (p *Parser) parseWhereClause() (*WhereComponent, error) {

	var whereComponent WhereComponent

	var field, operator, value string

	token, err := p.lexer.GetToken()
	if err != nil {
		return &whereComponent, err
	}

ReadWhereClauseLoop:
	for {

		if token.Type != TokenTypeField {
			if err != nil {
				return &whereComponent, errors.New("Expected TokenTypeField but found" + token.Value)
			}
		}

		field = token.Value

		token, err = p.lexer.GetToken()
		if err != nil {
			return &whereComponent, err
		}

		if token.Type != TokenTypeSymbol {
			if err != nil {
				return &whereComponent, errors.New("Expected TokenTypeSymbol but found" + token.Value)
			}
		}

		operator = token.Value

		token, err = p.lexer.GetToken()
		if err != nil {
			return &whereComponent, err
		}

		if token.Type != TokenTypeString && token.Type != TokenTypeNumber {
			if err != nil {
				return &whereComponent, errors.New("Expected TokenTypeString|TokenTypeNumber but found" + token.Value)
			}
		}

		value = token.Value

		whereComponent.Filters = append(whereComponent.Filters, Filter{field, operator, value})

		token, err = p.lexer.GetToken()
		if err != nil {
			if err == io.EOF {
				err = nil
			}

			return &whereComponent, err
		}

		switch token.Value {
		case "AND", "OR":
			// Handle AND/OR
			break
		default:
			p.lexer.UngetToken(token)
			break ReadWhereClauseLoop
		}

		token, err = p.lexer.GetToken()
		if err != nil {
			return &whereComponent, err
		}
	}

	return &whereComponent, nil
}

func (p *Parser) parseOrderClause() (*OrderComponent, error) {

	var orderComponent OrderComponent

	var field, direction string

	token, err := p.lexer.GetToken()
	if err != nil {
		return &orderComponent, err
	}

	if token.Type != TokenTypeKeyword {
		if err != nil {
			return &orderComponent, errors.New("Expected TokenTypeField but found" + token.Value)
		}
	}

	if token.Value != "BY" {
		if err != nil {
			return &orderComponent, errors.New("Expected BY but found" + token.Value)
		}
	}

	token, err = p.lexer.GetToken()
	if err != nil {
		return &orderComponent, err
	}

	if token.Type != TokenTypeField {
		if err != nil {
			return &orderComponent, errors.New("Expected TokenTypeField but found" + token.Value)
		}
	}

	field = token.Value

	token, err = p.lexer.GetToken()
	if err != nil {
		if err == io.EOF {
			err = nil
		}
		orderComponent.Orders = append(orderComponent.Orders, Order{field, "ASC"})
		return &orderComponent, err
	}

	if token.Type != TokenTypeKeyword {
		if err != nil {
			return &orderComponent, errors.New("Expected TokenTypeSymbol but found" + token.Value)
		}
	}

	direction = token.Value

	orderComponent.Orders = append(orderComponent.Orders, Order{field, direction})

	return &orderComponent, nil
}

func (p *Parser) parseLimitClause() (*LimitComponent, error) {

	var limitComponent LimitComponent

	var from, max int

	// get from value

	token, err := p.lexer.GetToken()
	if err != nil {
		if err == io.EOF {
			err = errors.New("EOF in LIMIT statement")
		}
		return &limitComponent, err
	}

	if token.Type != TokenTypeNumber {
		if err != nil {
			return &limitComponent, errors.New("Expected TokenTypeNumber but found" + token.Value)
		}
	}

	from, err = strconv.Atoi(token.Value)
	if err != nil {
		return &limitComponent, errors.New("Expected integer value but found" + token.Value)
	}

	// get comma?
	token, err = p.lexer.GetToken()

	if err != nil {
		if err == io.EOF {
			limitComponent.Limits = append(limitComponent.Limits, Limit{Max: from})
			return &limitComponent, nil
		}
		return &limitComponent, err
	}

	if token.Value != "," {
		p.lexer.UngetToken(token)
		limitComponent.Limits = append(limitComponent.Limits, Limit{Max: from})
		return &limitComponent, nil
	}
	token, err = p.lexer.GetToken()
	if err != nil {
		return &limitComponent, err
	}

	if token.Type != TokenTypeNumber {
		if err != nil {
			return &limitComponent, errors.New("Expected TokenTypeNumber but found" + token.Value)
		}
	}

	max, err = strconv.Atoi(token.Value)
	if err != nil {
		return &limitComponent, errors.New("Expected integer value but found" + token.Value)
	}

	limitComponent.Limits = append(limitComponent.Limits, Limit{from, max})

	return &limitComponent, nil
}

func (p *Parser) parseUpdateStatement(token *Token) (*SelectStatement, error) {
	return nil, errors.New("Not implemented")
}
