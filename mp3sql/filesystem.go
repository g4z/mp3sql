package mp3sql

import (
	"path/filepath"
	"strings"
)

// FindFiles finds mp3 files in the given directory
func FindFiles(directory string) ([]string, error) {

	pattern := strings.TrimRight(directory, "/") + "/*.mp3"

	filelist, err := filepath.Glob(pattern)
	if err != nil {
		return nil, err
	}

	return filelist, nil
}
