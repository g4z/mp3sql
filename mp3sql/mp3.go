package mp3sql

import (
	"github.com/bogem/id3v2"
)

// Mp3 models an mp3
type Mp3 struct {
	Artist string
	Title  string
	Genre  string
}

// GetTags reads ID3v2 tags from an mp3 file
func GetTags(filepath string) (*Mp3, error) {

	tag, err := id3v2.Open(filepath, id3v2.Options{Parse: true})
	if err != nil {
		return nil, err
	}
	defer tag.Close()

	mp3 := &Mp3{
		tag.Artist(),
		tag.Title(),
		tag.Genre(),
	}

	return mp3, nil

}

// Read frames.
// fmt.Println()
// fmt.Println(tag.Title())

// Set simple text frames.
// tag.SetArtist("New artist")
// tag.SetTitle("New title")

// // Set comment frame.
// comment := id3v2.CommentFrame{
// 	Encoding:    id3v2.ENUTF8,
// 	Language:    "eng",
// 	Description: "My opinion",
// 	Text:        "Very good song",
// }
// tag.AddCommentFrame(comment)

// // Write it to file.
// if err = tag.Save(); err != nil {
// 	return err
// }
