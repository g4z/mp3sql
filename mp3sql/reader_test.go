package mp3sql

import (
	"strconv"
	"testing"
)

func TestGetChar(t *testing.T) {

	var sql = "SELECT artist,title FROM ~/Music WHERE genre='House' AND year<2000"

	var reader *Reader
	var char *rune
	var err error

	reader, err = NewReader(sql)
	if err != nil {
		t.Error(err)
	}

	char, err = reader.GetChar()
	if err != nil {
		t.Error(err)
	}

	if *char != 'S' {
		t.Error("Expected S but found", string(*char))
	}

	for i := 0; i < 4; i++ {
		reader.GetChar()
	}

	char, err = reader.GetChar()
	if err != nil {
		t.Error(err)
	}

	if *char != 'T' {
		t.Error("Expected T but found", string(*char))
	}
}

func TestUngetChar(t *testing.T) {

	var sql = "SELECT artist,title FROM ~/Music WHERE genre='House' AND year<2000"

	var reader *Reader
	var char *rune
	var err error

	reader, err = NewReader(sql)
	if err != nil {
		t.Error(err)
	}

	for i := 0; i < 5; i++ {
		reader.GetChar()
	}

	char, err = reader.GetChar()
	if err != nil {
		t.Error(err)
	}

	if *char != 'T' {
		t.Error("Expected T but found", string(*char))
	}

	reader.UngetChar()

	char, err = reader.GetChar()
	if err != nil {
		t.Error(err)
	}

	if *char != 'T' {
		t.Error("Expected T but found", string(*char))
	}

	reader.UngetChar()
	reader.UngetChar()

	char, err = reader.GetChar()
	if err != nil {
		t.Error(err)
	}

	if *char != 'C' {
		t.Error("Expected C but found", string(*char))
	}
}

func TestEof(t *testing.T) {

	var sql = "SELECT artist,title FROM ~/Music WHERE genre='House' AND year<2000"

	var reader *Reader
	var err error

	reader, err = NewReader(sql)
	if err != nil {
		t.Error(err)
	}

	for i := 0; i < len(sql); i++ {
		reader.GetChar()
	}

	_, err = reader.GetChar()
	if err == nil {
		t.Error("Expected EOF")
	}
}

func TestLength(t *testing.T) {

	var sql = "SELECT artist,title FROM ~/Music WHERE genre='House' AND year<2000"

	var reader *Reader
	var err error

	reader, err = NewReader(sql)
	if err != nil {
		t.Error(err)
	}

	if reader.Length() != 66 {
		t.Error("Expected buffer length 66 not", strconv.Itoa(reader.Length()))
	}
}

func TestOffset(t *testing.T) {

	var sql = "SELECT artist,title FROM ~/Music WHERE genre='House' AND year<2000"

	var reader *Reader
	var err error

	reader, err = NewReader(sql)
	if err != nil {
		t.Error(err)
	}

	for i := 0; i < len(sql)-2; i++ {
		reader.GetChar()
	}

	if reader.Offset() != 64 {
		t.Error("Expected offset 64 not", strconv.Itoa(reader.Offset()))
	}
}
