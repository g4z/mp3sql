
# Development Notes

## Unit Tests
```bash
# run all unit tests
go test gitlab.com/g4z/mp3sql/mp3sql -v
```

## Usage Thoughts
```bash
mp3sql [sql]
```

### Sample SQL
```sql
SELECT artist,title FROM $HOME/Music WHERE genre='House' ORDER BY artist DESC LIMIT 10
```

### Options

    -c  case sensitive mode
    -d  enable debug mode
    -h  show help and exit
    -i  ignore files with file permission issues
    -v  show current version
    -m  mode (mp3, flac, etc...)

## SQL

### Supported Operations

    SELECT
    UPDATE

### Query Components

    SELECT Component
    UPDATE Component
    SET Component
    FROM Component
    WHERE Component
    ORDER Component
    LIMIT Component

### Keyword Identifiers

    SELECT
    UPDATE
    SET
    FROM
    WHERE
    OR
    AND
    ORDER BY
    ASC
    DESC
    LIMIT

### Field Identifiers

    # tag fields
    artist
    title
    album
    genre

    # metadata fields
    filename
    filepath
    filesize
    length?
    format?

### Values

    string      # eg. "a string", 'electronic'
    number      # eg. 1997

### Token Types

	Keyword
	Field
	String
	Number
	Symbol

### Syntax

#### SELECT

    SELECT $field [,$field2] ...

#### UPDATE

    UPDATE $dirpath [,$dirpath2] ...

#### FROM

    FROM $dirpath [,$dirpath2] ...

#### SET

    SET $field = $value [,$field2 = $value2] ...

#### WHERE

    WHERE $field $operator $value [AND|OR] ...

    # valid operators
    =
    !=
    ~       # LIKE
    !~      # NOT LIKE
    >
    <
    >=
    <=

#### ORDER

    ORDER BY $field [ASC|DESC]

#### LIMIT

    LIMIT [$from,] $max

### Example Queries

    SELECT * FROM ~/Music WHERE genre='Jazz' ORDER BY artist DESC LIMIT 10
    UPDATE ~/Music SET album='Whatever' WHERE filename !~ '%bob marley%'
    SELECT artist,title FROM ~/Music WHERE genre='House' AND year<2000

### Example SELECT statement breakdown


    SELECT artist,title FROM ~/Music,/music WHERE genre="Rock" AND year>1990 ORDER BY artist ASC LIMIT 10,5
    \_________________/ \_________________/ \______________________________/ \_________________/ \________/
       select clause        from clause               where clause              order clause       limit clause
    \____/ \__________/ \__/ \____________/ \___/ \________________________/ \______/ \____/ \_/ \___/ \__/
    action   fieldlist         sourcelist                 filters                       fl
                                                  \_________/      \_______/


    SELECT                      # identifier/keyword/action
    artist,title                # field list
    artist                      # identifier/field
    ,                           # symbol/delimiter
    title                       # identifier/field

    FROM                        # identifier/keyword
    ~/Music,/music              # source list
    ~/Music                     # source
    ,                           # symbol/delimiter
    /music                      # source

    WHERE                       # identifier/keyword
    genre="Rock" AND year>1990  # filter list
    genre="Rock"                # filter
    genre                       # identifier/field
    =                           # symbol/operator
    "Rock"                      # value/string
    AND                         # identifier/keyword
    year                        # identifier/field
    >                           # symbol/operator
    1990                        # value/number

    ORDER BY artist ASC         # order clause
    ORDER                       # identifier/keyword
    BY                          # identifier/keyword
    artist                      # identifier/field
    ASC                         # identifier/keyword

    LIMIT 10,5                  # limit clause
    LIMIT                       # identifier/keyword
    10                          # value/number
    ,                           # symbol/delimiter
    5                           # value/number


## 3rd party libs

- https://github.com/bogem/id3v2
- https://github.com/spf13/cobra
