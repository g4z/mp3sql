package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/g4z/mp3sql/mp3sql"
)

var rootCmd = &cobra.Command{
	Use:   "mp3sql [sql]",
	Args:  cobra.ExactArgs(1),
	Short: "MP3 management using SQL",
	Run: func(cmd *cobra.Command, args []string) {

		parser, err := mp3sql.NewParser(args[0])
		if err != nil {
			log.Fatal(err)
		}

		statement, err := parser.Parse()
		if err != nil {
			log.Fatal(err)
		}

		// fmt.Printf("SELECT: %v\n", statement.SelectComponent.Fields)
		// fmt.Printf("FROM: %v\n", statement.FromComponent.Sources)
		// fmt.Printf("WHERE: %v\n", statement.WhereComponent.Filters)
		// fmt.Printf("ORDER BY: %v\n", statement.OrderComponent.Orders)
		// fmt.Printf("LIMIT: %v\n", statement.LimitComponent.Limits)

		var collection []string

		for _, source := range statement.FromComponent.Sources {
			filepaths, err := mp3sql.FindFiles(source.Path)
			if err != nil {
				log.Fatal(err)
			}
			collection = append(collection, filepaths...)
		}

		// mp3sql.Dump(collection)

		var results []string

		for _, filepath := range collection {

			mp3, err := mp3sql.GetTags(filepath)
			if err != nil {
				log.Println(err)
			}

			for _, filter := range statement.WhereComponent.Filters {
				switch filter.Field {
				case "artist":
					if strings.ToUpper(mp3.Artist) == strings.ToUpper(filter.Value) {
						results = append(results, mp3.Artist+" - "+mp3.Title)
					}
				case "title":
					if strings.ToUpper(mp3.Title) == strings.ToUpper(filter.Value) {
						results = append(results, mp3.Artist+" - "+mp3.Title)
					}
				case "genre":
					if strings.ToUpper(mp3.Genre) == strings.ToUpper(filter.Value) {
						results = append(results, mp3.Artist+" - "+mp3.Title)
					}
				}
			}
		}

		for _, result := range results {
			fmt.Printf("%s\n", result)
		}
	},
}

func init() {
	// rootCmd.Flags().BoolP("ignorecase", "i", true, "Help message for toggle")
}

// Execute runs the command
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
